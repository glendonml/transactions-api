package com.pismo.accounts.domain

import spock.lang.Specification

class CalculatorResultSpec extends Specification {

  def "should sum the total"() {
    given: "a result with three changes"
    CalculatorResult calculatorResult = new CalculatorResult()
    calculatorResult.addNewChange(BigDecimal.TEN, null)
    calculatorResult.addNewChange(BigDecimal.TEN, null)
    calculatorResult.addNewChange(BigDecimal.TEN, null)

    when:
    def balance = calculatorResult.getTotalBalance()

    then:
    balance == new BigDecimal(30)
  }

}
