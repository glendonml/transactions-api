package com.pismo.accounts.domain

import spock.lang.Specification

class TransactionSpec extends Specification {

    def "should return 0 when balance is equal credit"() {
        given:
        Transaction transaction = new Transaction(balance: new BigDecimal(-100))

        when:
        def credit = transaction.amortize(100)

        then:
        credit == BigDecimal.ZERO
        transaction.balance == BigDecimal.ZERO
    }

    def "should return 0 when balance is bigger then credit"() {
        given:
        Transaction transaction = new Transaction(balance: new BigDecimal(-200))

        when:
        def credit = transaction.amortize(100)

        then:
        credit == BigDecimal.ZERO
        transaction.balance == new BigDecimal(-100)
    }

    def "should return credit when credit is bigger then balance"() {
        given:
        Transaction transaction = new Transaction(balance: new BigDecimal(-100))

        when:
        def credit = transaction.amortize(200)

        then:
        credit == new BigDecimal(100)
        transaction.balance == BigDecimal.ZERO
    }

}
