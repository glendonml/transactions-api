package com.pismo.accounts.fixtures

import br.com.six2six.fixturefactory.Fixture
import br.com.six2six.fixturefactory.Rule
import br.com.six2six.fixturefactory.loader.TemplateLoader
import com.pismo.accounts.domain.OperationType
import com.pismo.accounts.domain.Transaction

import java.math.MathContext
import java.time.ZonedDateTime

class TransactionFixture implements TemplateLoader {

  static final String TO_CREATE = "to-create"

  @Override
  void load() {
    Fixture.of(Transaction.class).addTemplate(TO_CREATE, new Rule() {
      {
        add("accountId", random(Long, range(1, 10)))
        add("operationType", random(OperationType.PAGAMENTO, OperationType.SAQUE,
                OperationType.COMPRA_A_VISTA, OperationType.COMPRA_PARCELADA))
        add("amount", random(BigDecimal, new MathContext(2)))
        add("balance", random(BigDecimal, new MathContext(2)))
        add("eventDate", ZonedDateTime.now())
        add("dueDate", ZonedDateTime.now())
      }
    })
  }
}
