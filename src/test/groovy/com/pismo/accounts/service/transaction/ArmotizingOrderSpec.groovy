package com.pismo.accounts.service.transaction

import com.pismo.accounts.domain.OperationType
import com.pismo.accounts.domain.Transaction
import spock.lang.Specification

import java.time.ZonedDateTime

class ArmotizingOrderSpec extends Specification {

    def amortizingOrder

    def setup() {
        amortizingOrder = new AmortizingOrder()
    }

    def "should sort the transactions by priority and eventDate"() {
        given: " a transaction that is priority 1"
        def eventDate = ZonedDateTime.now()
        def priority1operation = OperationType.values().find { it -> it.chargeOrder == 1}
        def third = new Transaction(id: 1, operationType : priority1operation, eventDate : eventDate)

        and: " a transaction that is priority 2"
        def priority2operation = OperationType.values().find {it -> it.chargeOrder == 2}
        def fourth = new Transaction(id: 2, operationType : priority2operation, eventDate : eventDate)

        and: " a transaction that is priority 0 but older"
        def priority0operation = OperationType.values().find {it -> it.chargeOrder == 0}
        def first = new Transaction(id: 3, operationType : priority0operation, eventDate : eventDate.minusDays(1))

        and: " a transaction that is priority 0 and newer"
        def second = new Transaction(id: 4, operationType : priority0operation, eventDate : eventDate)

        def transactions = [third, fourth, first, second] as Set

        when:
        def sorted = amortizingOrder.sort(transactions)

        then:
        sorted[0].id == first.id
        sorted[1].id == second.id
        sorted[2].id == third.id
        sorted[3].id == fourth.id
    }
}
