package com.pismo.accounts.service.transaction

import com.pismo.accounts.domain.CalculatorResult
import com.pismo.accounts.domain.OperationType
import com.pismo.accounts.domain.Transaction
import com.pismo.accounts.repository.TransactionRepository
import spock.lang.Specification


class CreditCalculatorSpec extends Specification {

    def creditCalculator

    def transactionRepository = Mock(TransactionRepository)

    def amortizingOrder = Mock(AmortizingOrder)

    def setup() {
        creditCalculator = new CreditCalculator(transactionRepository, amortizingOrder)
    }

    def "should turn Zero all balances when the credit is equal to debit amount"() {
        given: "transactions with negative balance"
        def debits = [] as Set
        debits << new Transaction(balance: new BigDecimal(-23.53))
        debits << new Transaction(balance: new BigDecimal(-26.47))
        debits << new Transaction(balance: new BigDecimal(-20.00))
        debits << new Transaction(balance: new BigDecimal(-30.00))

        and: "a positive transaction"
        def credit = new Transaction(amount: new BigDecimal(100.00),
                operationType: OperationType.PAGAMENTO)

        amortizingOrder.sort(_) >> debits

        when:
        CalculatorResult result = creditCalculator.exec(credit)

        then:
        result.amount == BigDecimal.ZERO
        result.changedTransactions.size() == 4
        result.changedTransactions.transaction.balance.every {it -> it == BigDecimal.ZERO}
    }
}
