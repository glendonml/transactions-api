create table payment_tracking (
  id bigserial primary key,
  credit_transaction_id bigint references transaction (id)  not null,
  debit_transaction_id bigint references transaction (id)  not null,
  amount numeric(15,2) not null
)