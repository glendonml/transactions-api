create table transaction (
  id bigserial primary key,
  operation_type integer not null,
  account_id bigint not null,
  amount numeric(15,2) not null,
  balance numeric(15,2) not null,
  event_date timestamp not null,
  due_date timestamp
);

create index on transaction (account_id, operation_type);