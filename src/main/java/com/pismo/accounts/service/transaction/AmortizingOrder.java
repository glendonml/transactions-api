package com.pismo.accounts.service.transaction;

import com.pismo.accounts.domain.Transaction;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class AmortizingOrder {

  public Set<Transaction> sort(Set<Transaction> transactions) {

    Comparator<Transaction> byChargerOrder = Comparator
        .comparingInt(transaction -> transaction.getOperationType().getChargeOrder());

    Comparator<Transaction> byEventDate = Comparator.comparing(Transaction::getEventDate);

    return transactions.stream().sorted(byChargerOrder.thenComparing(byEventDate)).collect(
        Collectors.toCollection(LinkedHashSet::new)) ;
  }

}
