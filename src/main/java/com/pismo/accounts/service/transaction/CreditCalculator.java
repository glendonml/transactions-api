package com.pismo.accounts.service.transaction;

import com.pismo.accounts.domain.CalculatorResult;
import com.pismo.accounts.domain.OperationType;
import com.pismo.accounts.domain.Transaction;
import com.pismo.accounts.repository.TransactionRepository;
import java.math.BigDecimal;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreditCalculator implements Calculator {

  private final TransactionRepository transactionRepository;
  private final AmortizingOrder amortizingOrder;

  @Autowired
  public CreditCalculator(TransactionRepository transactionRepository,
      AmortizingOrder amortizingOrder) {
    this.transactionRepository = transactionRepository;
    this.amortizingOrder = amortizingOrder;
  }

  @Override
  public CalculatorResult exec(Transaction transaction) {
    CalculatorResult calculatorResult = new CalculatorResult();
    BigDecimal credit = transaction.getAmount();

    if (transaction.getOperationType() == OperationType.PAGAMENTO) {
      Set<Transaction> transactionsToAmortize = amortizingOrder.sort(
          transactionRepository.findToAmortize(
              transaction.getAccountId(), OperationType.PAGAMENTO));

      for (Transaction toAmortize : transactionsToAmortize) {
        BigDecimal finalCredit = toAmortize.amortize(credit);
        calculatorResult.addNewChange(credit.subtract(finalCredit), toAmortize);
        credit = finalCredit;
        if (credit.equals(BigDecimal.ZERO)) {
          break;
        }
      }
    }
    calculatorResult.setAmount(credit);

    return calculatorResult;
  }
}
