package com.pismo.accounts.service.transaction;

import com.pismo.accounts.domain.CalculatorResult;
import com.pismo.accounts.domain.Transaction;

public interface Calculator {
  CalculatorResult exec(Transaction transaction);
}
