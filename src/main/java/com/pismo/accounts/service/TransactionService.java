package com.pismo.accounts.service;

import com.pismo.accounts.client.accounts.AccountsApiClient;
import com.pismo.accounts.domain.OperationType;
import com.pismo.accounts.domain.Transaction;
import com.pismo.accounts.repository.PaymentTrackingRepository;
import com.pismo.accounts.repository.TransactionRepository;
import com.pismo.accounts.domain.CalculatorResult;
import com.pismo.accounts.service.transaction.CreditCalculator;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TransactionService {

  private final TransactionRepository transactionRepository;

  private final PaymentTrackingRepository paymentTrackingRepository;

  private final CreditCalculator creditCalculator;

  private final AccountsApiClient accountsApiClient;

  @Autowired
  public TransactionService(
      TransactionRepository transactionRepository,
      PaymentTrackingRepository paymentTrackingRepository,
      CreditCalculator creditCalculator,
      AccountsApiClient accountsApiClient) {
    this.transactionRepository = transactionRepository;
    this.paymentTrackingRepository = paymentTrackingRepository;
    this.creditCalculator = creditCalculator;
    this.accountsApiClient = accountsApiClient;
  }

  public List<Transaction> create(List<Transaction> transactions) {
    return transactions.stream().map(this::create).collect(Collectors.toList());
  }

  public Transaction create(Transaction transaction) {
    transaction.setEventDate(ZonedDateTime.now());
    transaction.setDueDate(transaction.getEventDate().plusMonths(1).withDayOfMonth(10));
    transaction.setAmount(verifyOperation(transaction));

    CalculatorResult result = creditCalculator.exec(transaction);
    transaction.setBalance(result.getAmount());
    Transaction savedTransaction = transactionRepository.save(transaction);
    result.getChangedTransactions().forEach(it -> {
      transactionRepository.save(it.getTransaction());
      paymentTrackingRepository.save(it.generatePaymentTracking(savedTransaction));
    });
    accountsApiClient.updateAccountBalance(transaction.getAccountId(), result.getTotalBalance());

    return savedTransaction;
  }

  BigDecimal verifyOperation(Transaction transaction) {
    return transaction.getOperationType() != OperationType.PAGAMENTO
        ? transaction.getAmount().negate()
        : transaction.getAmount();
  }

}
