package com.pismo.accounts.domain;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class PaymentTracking {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "credit_transaction_id", referencedColumnName = "id")
  private Transaction creditTransaction;

  @ManyToOne
  @JoinColumn(name = "debit_transaction_id", referencedColumnName = "id")
  private Transaction debitTransaction;

  private BigDecimal amount;

  public PaymentTracking() {
    super();
  }

  public PaymentTracking(Transaction creditTransaction,
      Transaction debitTransaction, BigDecimal amount) {
    this.creditTransaction = creditTransaction;
    this.debitTransaction = debitTransaction;
    this.amount = amount;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Transaction getCreditTransaction() {
    return creditTransaction;
  }

  public void setCreditTransaction(Transaction creditTransaction) {
    this.creditTransaction = creditTransaction;
  }

  public Transaction getDebitTransaction() {
    return debitTransaction;
  }

  public void setDebitTransaction(Transaction debitTransaction) {
    this.debitTransaction = debitTransaction;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }
}
