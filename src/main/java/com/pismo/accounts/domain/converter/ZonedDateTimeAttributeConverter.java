package com.pismo.accounts.domain.converter;

import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ZonedDateTimeAttributeConverter
    implements AttributeConverter<ZonedDateTime, Timestamp> {

  @Override
  public Timestamp convertToDatabaseColumn(ZonedDateTime attribute) {
    return attribute == null
        ? null
        : Timestamp.valueOf(attribute.toLocalDateTime());
  }

  @Override
  public ZonedDateTime convertToEntityAttribute(Timestamp dbData) {
    return dbData == null
        ? null
        : dbData.toLocalDateTime().atZone(ZoneId.systemDefault());
  }
}
