package com.pismo.accounts.domain;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Transaction {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  private Long accountId;

  @NotNull
  @Enumerated(EnumType.ORDINAL)
  private OperationType operationType;

  @NotNull
  private BigDecimal amount;

  @NotNull
  private BigDecimal balance;

  @NotNull
  private ZonedDateTime eventDate;

  private ZonedDateTime dueDate;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getAccountId() {
    return accountId;
  }

  public void setAccountId(Long accountId) {
    this.accountId = accountId;
  }

  public OperationType getOperationType() {
    return operationType;
  }

  public void setOperationType(OperationType operationType) {
    this.operationType = operationType;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  public ZonedDateTime getEventDate() {
    return eventDate;
  }

  public void setEventDate(ZonedDateTime eventDate) {
    this.eventDate = eventDate;
  }

  public ZonedDateTime getDueDate() {
    return dueDate;
  }

  public void setDueDate(ZonedDateTime dueDate) {
    this.dueDate = dueDate;
  }

  public BigDecimal amortize(BigDecimal credit) {
    BigDecimal newCredit;

    if (credit.compareTo(balance.negate()) >= 0) {
      newCredit = credit.add(balance);
      balance = BigDecimal.ZERO;
    } else {
      balance = balance.add(credit);
      newCredit = BigDecimal.ZERO;
    }
    return newCredit;
  }
}
