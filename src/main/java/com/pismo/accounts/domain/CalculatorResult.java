package com.pismo.accounts.domain;

import com.pismo.accounts.domain.PaymentTracking;
import com.pismo.accounts.domain.Transaction;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class CalculatorResult {

  private BigDecimal amount;

  private Set<Changed> changedTransactions;

  public CalculatorResult() {
    this.amount = BigDecimal.ZERO;
    this.changedTransactions = new HashSet<>();
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public Set<Changed> getChangedTransactions() {
    return changedTransactions;
  }

  public void addNewChange(BigDecimal total, Transaction transaction) {
    changedTransactions.add(new Changed(total, transaction));
  }

  public class Changed {
    private final BigDecimal total;
    private final Transaction transaction;

    public Changed(BigDecimal total, Transaction transaction) {
      this.total = total;
      this.transaction = transaction;
    }

    public Transaction getTransaction() {
      return transaction;
    }

    public BigDecimal getTotal() {
      return total;
    }

    public PaymentTracking generatePaymentTracking(Transaction creditTransaction) {
      return new PaymentTracking(creditTransaction, transaction, total);
    }
  }

  public BigDecimal getTotalBalance() {
    return this.changedTransactions.stream()
        .map(i -> i.total)
        .reduce(BigDecimal.ZERO, BigDecimal::add);
  }
}
