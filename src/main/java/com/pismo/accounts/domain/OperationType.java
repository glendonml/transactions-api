package com.pismo.accounts.domain;

public enum OperationType {

  COMPRA_A_VISTA(1, 2),
  COMPRA_PARCELADA(2, 1),
  SAQUE(3, 0),
  PAGAMENTO(4, 0);

  OperationType(Integer id, Integer chargeOrder) {
    this.id = id;
    this.chargeOrder = chargeOrder;
  }

  private Integer id;

  private Integer chargeOrder;

  public Integer getId() {
    return id;
  }

  public Integer getChargeOrder() {
    return chargeOrder;
  }

  public static OperationType getById(Integer id) {
    for (OperationType operationType : OperationType.values()) {
      if (operationType.id == id) {
        return operationType;
      }
    }
    throw new IllegalArgumentException("Invalid operation type");
  }

}
