package com.pismo.accounts.repository;

import com.pismo.accounts.domain.OperationType;
import com.pismo.accounts.domain.Transaction;
import java.util.Set;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

  @Query("SELECT t FROM Transaction t "
      + "WHERE t.accountId = ?1 "
      + "AND t.balance != 0 "
      + "AND t.operationType != ?2")
  Set<Transaction> findToAmortize(Long accountId, OperationType debitOperation);

  @Query("SELECT t FROM Transaction t "
      + "WHERE t.accountId = ?1 "
      + " order by t.eventDate")
  Set<Transaction> findByAccountId(Long accountId);
}
