package com.pismo.accounts.repository;

import com.pismo.accounts.domain.PaymentTracking;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentTrackingRepository extends CrudRepository<PaymentTracking, Long> {
}
