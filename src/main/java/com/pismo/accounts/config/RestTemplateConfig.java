package com.pismo.accounts.config;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {

  @Bean
  RestTemplate restTemplate(HttpComponentsClientHttpRequestFactory clientHttpRequest) {
    return new RestTemplate(clientHttpRequest);
  }

  @Bean
  HttpComponentsClientHttpRequestFactory clientHttpRequestFactory() {
    CloseableHttpClient client = HttpClientBuilder.create().build();
    return new HttpComponentsClientHttpRequestFactory(client);
  }
}
