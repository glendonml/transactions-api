package com.pismo.accounts.controller.v1;

import com.pismo.accounts.domain.Transaction;
import com.pismo.accounts.repository.TransactionRepository;
import com.pismo.accounts.resource.v1.AccountTransactionsRespose;
import com.pismo.accounts.resource.v1.PaymentRequest;
import com.pismo.accounts.resource.v1.TransactionRequest;
import com.pismo.accounts.service.TransactionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1")
@Api(value = "transactions", description = "Transaction operations")
public class TransactionController {

  private final TransactionService transactionService;
  private final TransactionRepository transactionRepository;

  @Autowired
  public TransactionController(TransactionService transactionService,
      TransactionRepository transactionRepository) {
    this.transactionService = transactionService;
    this.transactionRepository = transactionRepository;
  }

  @PostMapping("/transactions")
  @ApiOperation(value = "Create general purpose transactions")
  public ResponseEntity createTransaction(@RequestBody TransactionRequest request) {
    transactionService.create(request.generateTransaction());
    return ResponseEntity.status(HttpStatus.CREATED).build();
  }

  @PostMapping("/payments")
  @ApiOperation(value = "Create only payments, it's possible to insert one or more")
  public ResponseEntity createPayment(@RequestBody List<PaymentRequest> request) {

    List<Transaction> transactions = request.stream()
        .map(PaymentRequest::generateTransaction)
        .collect(Collectors.toList());

    transactionService.create(transactions);
    return ResponseEntity.status(HttpStatus.CREATED).build();
  }

  @GetMapping("/transactions/account/{accountId}")
  @ApiOperation(value = "Get account transactions - to make easier to validate :) ")
  ResponseEntity get(@PathVariable("accountId") Long accountId) {
    Set<Transaction> transactions = transactionRepository.findByAccountId(accountId);
    return ResponseEntity.ok(transactions.stream()
        .map(t -> new AccountTransactionsRespose(t))
        .collect(Collectors.toList()));
  }

}
