package com.pismo.accounts.resource.v1;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pismo.accounts.domain.OperationType;
import com.pismo.accounts.domain.Transaction;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;

public class TransactionRequest {

  @JsonProperty(value = "account_id", required = true)
  @ApiModelProperty(required = true)
  private Long accountId;

  @JsonProperty(value = "operation_type_id", required = true)
  @ApiModelProperty(allowableValues = "1, 2, 3, 4", required = true,
      notes = "1 - COMPRA A VISTA, 2 - COMPRA PARCELADA, 3 - SAQUE, 4 - PAGAMENTO")
  private Integer operationTypeId;

  @JsonProperty(required = true)
  @ApiModelProperty(required = true)
  private BigDecimal amount;

  public Long getAccountId() {
    return accountId;
  }

  public void setAccountId(Long accountId) {
    this.accountId = accountId;
  }

  public Integer getOperationTypeId() {
    return operationTypeId;
  }

  public void setOperationTypeId(Integer operationTypeId) {
    this.operationTypeId = operationTypeId;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public Transaction generateTransaction() {
    Transaction transaction = new Transaction();
    transaction.setAccountId(accountId);
    transaction.setOperationType(OperationType.getById(operationTypeId));
    transaction.setAmount(amount);

    return transaction;
  }
}
