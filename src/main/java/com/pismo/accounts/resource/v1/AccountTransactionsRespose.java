package com.pismo.accounts.resource.v1;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pismo.accounts.domain.Transaction;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class AccountTransactionsRespose {

  @JsonProperty(value = "transaction_id")
  private Long transactionId;

  @JsonProperty(value = "account_id")
  private Long accountId;

  @JsonProperty(value = "operation_type_id")
  private Integer operationTypeId;

  private BigDecimal amount;

  private BigDecimal balance;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
  private ZonedDateTime eventDate;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
  private ZonedDateTime dueDate;

  public AccountTransactionsRespose(Transaction transaction) {
    this.transactionId = transaction.getId();
    this.accountId = transaction.getAccountId();
    if (transaction.getOperationType() != null) {
      this.operationTypeId = transaction.getOperationType().getId();
    }
    this.amount = transaction.getAmount();
    this.balance = transaction.getBalance();
    this.eventDate = transaction.getEventDate();
    this.dueDate = transaction.getDueDate();
  }

  public Long getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(Long transactionId) {
    this.transactionId = transactionId;
  }

  public Long getAccountId() {
    return accountId;
  }

  public void setAccountId(Long accountId) {
    this.accountId = accountId;
  }

  public Integer getOperationTypeId() {
    return operationTypeId;
  }

  public void setOperationTypeId(Integer operationTypeId) {
    this.operationTypeId = operationTypeId;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  public ZonedDateTime getEventDate() {
    return eventDate;
  }

  public void setEventDate(ZonedDateTime eventDate) {
    this.eventDate = eventDate;
  }

  public ZonedDateTime getDueDate() {
    return dueDate;
  }

  public void setDueDate(ZonedDateTime dueDate) {
    this.dueDate = dueDate;
  }
}
