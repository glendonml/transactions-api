package com.pismo.accounts.resource.v1;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pismo.accounts.domain.OperationType;
import com.pismo.accounts.domain.Transaction;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;

public class PaymentRequest {

  @JsonProperty(value = "account_id", required = true)
  @ApiModelProperty(required = true)
  private Long accountId;

  @JsonProperty(required = true)
  @ApiModelProperty(required = true)
  private BigDecimal amount;

  public Long getAccountId() {
    return accountId;
  }

  public void setAccountId(Long accountId) {
    this.accountId = accountId;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public Transaction generateTransaction() {
    Transaction transaction = new Transaction();
    transaction.setAccountId(accountId);
    transaction.setOperationType(OperationType.PAGAMENTO);
    transaction.setAmount(amount);

    return transaction;
  }
}
