package com.pismo.accounts.client.accounts;

import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Component
public class AccountsApiClient {

  private final String basePath;
  private final RestTemplate restTemplate;

  @Autowired
  public AccountsApiClient(
      @Value("${acccounts.api.url}") String basePath,
      RestTemplate restTemplate) {
    this.basePath = basePath;
    this.restTemplate = restTemplate;
  }

  public void updateAccountBalance(Long accountId, BigDecimal total) {
    AccountRequest request = new AccountRequest(total);
    try {
      restTemplate.patchForObject(String.format("%s/v1/accounts/%d", basePath, accountId),
          request, String.class);
    } catch (HttpClientErrorException exception) {
      throw exception;
    }
  }

}
