package com.pismo.accounts.client.accounts;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

public class AccountRequest {

  @JsonProperty(value = "available_credit_limit")
  private Amount availableCreditLimit;

  @JsonProperty(value = "available_withdrawal_limit")
  private Amount availableWithdrawalLimit;

  public AccountRequest(BigDecimal value) {
    this.availableCreditLimit = new Amount(value);
    this.availableWithdrawalLimit = new Amount(value);
  }

  public Amount getAvailableCreditLimit() {
    return availableCreditLimit;
  }

  public void setAvailableCreditLimit(
      Amount availableCreditLimit) {
    this.availableCreditLimit = availableCreditLimit;
  }

  public Amount getAvailableWithdrawalLimit() {
    return availableWithdrawalLimit;
  }

  public void setAvailableWithdrawalLimit(
      Amount availableWithdrawalLimit) {
    this.availableWithdrawalLimit = availableWithdrawalLimit;
  }

  public class Amount {
    BigDecimal amount;

    public Amount(BigDecimal amount) {
      this.amount = amount;
    }

    public BigDecimal getAmount() {
      return amount;
    }

    public void setAmount(BigDecimal amount) {
      this.amount = amount;
    }
  }

}
