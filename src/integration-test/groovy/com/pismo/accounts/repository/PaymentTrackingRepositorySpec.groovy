package com.pismo.accounts.repository

import br.com.six2six.fixturefactory.Fixture
import com.pismo.accounts.BaseSpec
import com.pismo.accounts.domain.PaymentTracking
import com.pismo.accounts.domain.Transaction
import com.pismo.accounts.fixtures.FixtureLoader
import com.pismo.accounts.fixtures.TransactionFixture
import com.pismo.accounts.processor.TransactionProcessor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional

@Transactional
class PaymentTrackingRepositorySpec extends BaseSpec {

    @Autowired
    TransactionRepository transactionRepository

    @Autowired
    PaymentTrackingRepository paymentTrackingRepository

    TransactionProcessor transactionProcessor

    def setup() {
        FixtureLoader.loadTemplates()
        transactionProcessor = new TransactionProcessor(transactionRepository)
    }

    def "should create a paymentTracking"() {
        given: "a debit and credit operation"
        Transaction debit = Fixture.from(Transaction.class).uses(transactionProcessor)
                .gimme(TransactionFixture.TO_CREATE)
        Transaction credit = Fixture.from(Transaction.class).uses(transactionProcessor)
                .gimme(TransactionFixture.TO_CREATE)

        and:
        PaymentTracking paymentTracking = new PaymentTracking(credit, debit, BigDecimal.TEN)

        when:
        paymentTrackingRepository.save(paymentTracking)
        PaymentTracking saved =  paymentTrackingRepository.findOne(paymentTracking.id)

        then:
        saved != null
    }

    def "should update a paymentTracking"() {
        given:
        Transaction debit = Fixture.from(Transaction.class).uses(transactionProcessor)
                .gimme(TransactionFixture.TO_CREATE)
        Transaction credit = Fixture.from(Transaction.class).uses(transactionProcessor)
                .gimme(TransactionFixture.TO_CREATE)
        def saved = paymentTrackingRepository.save(
                new PaymentTracking(credit, debit, BigDecimal.TEN))

        and:
        saved.amount = BigDecimal.TEN

        when:
        paymentTrackingRepository.save(saved)
        PaymentTracking updated =  paymentTrackingRepository.findOne(saved.id)

        then:
        updated.amount == BigDecimal.TEN
    }

    def "should delete a paymentTracking"() {
        given:
        Transaction debit = Fixture.from(Transaction.class).uses(transactionProcessor)
                .gimme(TransactionFixture.TO_CREATE)
        Transaction credit = Fixture.from(Transaction.class).uses(transactionProcessor)
                .gimme(TransactionFixture.TO_CREATE)
        def saved = paymentTrackingRepository.save(
                new PaymentTracking(credit, debit, BigDecimal.TEN))

        when:
        paymentTrackingRepository.delete(saved)
        PaymentTracking deleted =  paymentTrackingRepository.findOne(saved.id)

        then:
        deleted == null
    }


}
