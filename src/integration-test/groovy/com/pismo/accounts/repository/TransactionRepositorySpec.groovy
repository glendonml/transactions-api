package com.pismo.accounts.repository

import br.com.six2six.fixturefactory.Fixture
import br.com.six2six.fixturefactory.Rule
import com.pismo.accounts.BaseSpec

import com.pismo.accounts.domain.OperationType
import com.pismo.accounts.domain.Transaction

import com.pismo.accounts.fixtures.FixtureLoader

import com.pismo.accounts.fixtures.TransactionFixture
import com.pismo.accounts.processor.TransactionProcessor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional

@Transactional
class TransactionRepositorySpec extends BaseSpec {

    @Autowired
    TransactionRepository transactionRepository

    TransactionProcessor transactionProcessor

    def setup() {
        FixtureLoader.loadTemplates()
        transactionProcessor = new TransactionProcessor(transactionRepository)
    }

    def "should create a transaction"() {
        given: "a new transaction"
        Transaction transaction = Fixture.from(Transaction.class)
                .gimme(TransactionFixture.TO_CREATE)

        when:
        transactionRepository.save(transaction)
        Transaction savedTransaction =  transactionRepository.findOne(transaction.id)

        then:
        savedTransaction != null
    }

    def "should update a transaction"() {
        given:
        Transaction transaction = Fixture.from(Transaction.class)
                .gimme(TransactionFixture.TO_CREATE)
        transactionRepository.save(transaction)

        when:"update the amount and balance"
        transaction.amount = BigDecimal.TEN
        transaction.balance = BigDecimal.TEN
        transactionRepository.save(transaction)
        Transaction updatedTransaction =  transactionRepository.findOne(transaction.id)

        then:
        updatedTransaction.amount == BigDecimal.TEN
        updatedTransaction.balance == BigDecimal.TEN
    }

    def "should delete a transaction"() {
        given:
        Transaction transaction = Fixture.from(Transaction.class)
                .gimme(TransactionFixture.TO_CREATE)
        transactionRepository.save(transaction)

        when:
        transactionRepository.delete(transaction)
        Transaction deletedTransaction =  transactionRepository.findOne(transaction.id)

        then:
        deletedTransaction == null
    }

    def "should find transactions to amortize when balance != 0 and operation is DEBIT"() {
        given: "an operation type"
        def debitOperationType = OperationType.SAQUE

        and: "and three DEBIT transactions for the same account using those operations"
        def accountId = 1L
        Transaction firstTransaction = Fixture.from(Transaction.class)
                .gimme(TransactionFixture.TO_CREATE)
        firstTransaction.accountId = accountId
        firstTransaction.operationType = debitOperationType
        firstTransaction.balance = -50
        transactionRepository.save(firstTransaction)

        Transaction lastTransaction = Fixture.from(Transaction.class)
                .gimme(TransactionFixture.TO_CREATE)
        lastTransaction.accountId = accountId
        lastTransaction.operationType = debitOperationType
        firstTransaction.balance = -40
        transactionRepository.save(lastTransaction)

        and: " a transaction from other account, that should not be found"
        Transaction notFoundTransaction = Fixture.from(Transaction.class)
                .gimme(TransactionFixture.TO_CREATE)
        notFoundTransaction.accountId = 2L
        notFoundTransaction.operationType = debitOperationType
        transactionRepository.save(notFoundTransaction)

        when:
        Set<Transaction> transactionsToAmortize =  transactionRepository
                .findToAmortize(accountId, OperationType.PAGAMENTO)

        then:
        transactionsToAmortize.size() == 2
        transactionsToAmortize.id.contains(firstTransaction.id)
        transactionsToAmortize.id.contains(lastTransaction.id)
        !transactionsToAmortize.id.contains(notFoundTransaction.id)

    }

    def "should not find transactions to amortize when balance is 0"() {
        given: "an operation type"
        def priorityOperationType = OperationType.SAQUE

        and: "and a transaction that has balance 0"
        def accountId = 1L
        Transaction transaction = Fixture.from(Transaction.class)
                .gimme(TransactionFixture.TO_CREATE)
        transaction.accountId = accountId
        transaction.operationType = priorityOperationType
        transaction.balance = 0
        transactionRepository.save(transaction)

        when:
        Set<Transaction> transactionsToAmortize =  transactionRepository
                .findToAmortize(accountId, OperationType.PAGAMENTO)

        then:
        transactionsToAmortize.isEmpty()
    }

    def "should not find CREDIT transaction to amortize"() {
        given: "an operation type"
        def creditOperationType = OperationType.PAGAMENTO

        and: "and a transaction that is a CREDIT"
        def accountId = 1L
        Transaction transaction = Fixture.from(Transaction.class)
                .gimme(TransactionFixture.TO_CREATE)
        transaction.accountId = accountId
        transaction.operationType = creditOperationType
        transaction.balance = 100
        transactionRepository.save(transaction)

        when:
        Set<Transaction> transactionsToAmortize =  transactionRepository
                .findToAmortize(accountId, OperationType.PAGAMENTO)

        then:
        transactionsToAmortize.isEmpty()
    }

    def "should find by Account Id"() {
        given: "transactions from the same account"
        Fixture.from(Transaction.class).uses(transactionProcessor)
                .gimme(2, TransactionFixture.TO_CREATE, new Rule(){
            {
                add("accountId", 10L)
            }
        })

        and:
        Fixture.from(Transaction.class).uses(transactionProcessor)
                .gimme(TransactionFixture.TO_CREATE, new Rule(){
            {
                add("accountId", 1L)
            }
        })

        when:
        Set<Transaction> transactionsFound = transactionRepository.findByAccountId(10L)

        then:
        transactionsFound.size() == 2
    }

}