package com.pismo.accounts.service

import br.com.six2six.fixturefactory.Fixture
import com.pismo.accounts.client.accounts.AccountsApiClient
import com.pismo.accounts.domain.OperationType
import com.pismo.accounts.domain.Transaction
import com.pismo.accounts.fixtures.FixtureLoader
import com.pismo.accounts.fixtures.TransactionFixture
import com.pismo.accounts.repository.PaymentTrackingRepository
import com.pismo.accounts.repository.TransactionRepository
import com.pismo.accounts.domain.CalculatorResult
import com.pismo.accounts.service.transaction.CreditCalculator
import spock.lang.Specification

class TransactionServiceSpec extends Specification {

    TransactionService transactionService

    def transactionRepository = Mock(TransactionRepository)

    def paymentTrackingRepository = Mock(PaymentTrackingRepository)

    def creditCalculator = Mock(CreditCalculator)

    def accountsApiClient = Mock(AccountsApiClient)

    def setup() {
        FixtureLoader.loadTemplates()
        transactionService = Spy(TransactionService, constructorArgs: [
                transactionRepository,
                paymentTrackingRepository,
                creditCalculator,
                accountsApiClient])
    }

    def "should set event date, due date and a new balance" () {
        given: "a new transaction"
        Transaction transaction = Fixture.from(Transaction.class)
                .gimme(TransactionFixture.TO_CREATE)

        and:
        CalculatorResult result = new CalculatorResult()
        result.amount = BigDecimal.TEN
        creditCalculator.exec(_) >> result

        when:
        def createdTransaction = transactionService.create(transaction)

        then:
        1 * transactionRepository.save(_) >> transaction
        createdTransaction.eventDate != null
        createdTransaction.dueDate == createdTransaction.eventDate.plusMonths(1).withDayOfMonth(10)
        createdTransaction.balance == BigDecimal.TEN
    }

    def "should crete a list of transactions" () {
        given: "a new transaction"
        List<Transaction> transactions = Fixture.from(Transaction.class)
                .gimme(10, TransactionFixture.TO_CREATE)

        when:
        def createdTransactions = transactionService.create(transactions)

        then:
        createdTransactions.size() == 10
        10 * transactionService.create(_ as Transaction) >> new Transaction()
    }

    def "should call creditCalculator and save all changed transactions when PAGAMENTO" (){
        given: "a payment transaction"
        def paymentTransaction = Fixture.from(Transaction.class)
                .gimme(TransactionFixture.TO_CREATE)
        paymentTransaction.operationType = OperationType.PAGAMENTO

        and:
        CalculatorResult result = new CalculatorResult()
        result.addNewChange(BigDecimal.TEN, new Transaction())
        result.addNewChange(BigDecimal.TEN, new Transaction())

        when:
        transactionService.create(paymentTransaction)

        then:
        1 * creditCalculator.exec(_) >> result
        2 * paymentTrackingRepository.save(_)
        //3 - one for the main transaction and two for the other.
        3 * transactionRepository.save(_)
        1 * accountsApiClient.updateAccountBalance(_, _)

    }

    def "should negate amount when is not PAGAMENTO"() {
        given: "a new transaction"
        Transaction transaction = Fixture.from(Transaction.class)
                .gimme(TransactionFixture.TO_CREATE)
        transaction.operationType = OperationType.COMPRA_PARCELADA

        and:
        transactionRepository.save(_) >> transaction

        and:
        creditCalculator.exec(_) >> new CalculatorResult()

        when:
        def initialAmount = transaction.amount
        def createdTransaction = transactionService.create(transaction)

        then:
        createdTransaction.amount == initialAmount.negate()
    }

    def "should keep amount when is PAGAMENTO"() {
        given: "a new transaction"
        Transaction transaction = Fixture.from(Transaction.class)
                .gimme(TransactionFixture.TO_CREATE)
        transaction.operationType = OperationType.PAGAMENTO

        and:
        transactionRepository.save(_) >> transaction

        and:
        creditCalculator.exec(_) >> new CalculatorResult()

        when:
        def initialAmount = transaction.amount
        def createdTransaction = transactionService.create(transaction)

        then:
        createdTransaction.amount == initialAmount
    }


}
