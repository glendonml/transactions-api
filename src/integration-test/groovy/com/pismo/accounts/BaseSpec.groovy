package com.pismo.accounts

import org.flywaydb.core.Flyway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification

import javax.sql.DataSource

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@SpringBootTest(webEnvironment = RANDOM_PORT)
abstract class BaseSpec extends Specification {

    @Autowired
    DataSource dataSource

    @Shared
    Boolean flywayInitialized

    @Autowired
    setupFlyway() {
        if (!flywayInitialized) {
            def flyway = new Flyway()
            flyway.dataSource = dataSource
            flyway.clean()
            flyway.migrate()
            flywayInitialized = true
        }
    }
}
