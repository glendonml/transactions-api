package com.pismo.accounts.processor;

import br.com.six2six.fixturefactory.processor.Processor
import com.pismo.accounts.repository.TransactionRepository;

class TransactionProcessor implements Processor{

    TransactionRepository transactionRepository

    TransactionProcessor(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository
    }

    @Override
    void execute(Object result) {
        transactionRepository.save(result)
    }
}
