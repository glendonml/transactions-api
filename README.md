# Transactions API

This projects is a sample project for Pismo. This project is focused on financial transactions on accounts. 

## Application configuration
 - Spring / Java 8
 - Swagger ([LOCAL](http://localhost:8080/swagger-ui.html#/))
 - PostgreSql

## How to Run

### IMPORTANT
  
  - All endpoints and how to use can be found on [SWAGGER](http://localhost:8080/swagger-ui.html#/)
  - Before start and run this service, you must run this project: [ACCOUNTS API](https://bitbucket.org/glendonml/accounts-api)

It's necessary to have Java 8 and Docker installed

 1. `docker-compose up --build -d`
 2. `./gradlew bootRun` 


## How to Test

 1. `docker-compose up --build -d`;
 2. `./gradlew check integrationTest`.
    1. `check`: run checkstyle and unit tests
    2. `integrationTest`: run integration tests
