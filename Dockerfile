FROM anapsix/alpine-java:latest

WORKDIR /opt/app/
ADD /build/libs/accounts-api.jar api.jar

EXPOSE 8080

ENV ENVIRONMENT=dev

CMD java -jar -Dspring.profiles.active=${ENVIRONMENT} api.jar